package com.example.auth.user.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserDTO {

	private int id;
	private String userName;
	private String password;
	private Integer age;
	private Long salary;
	private RoleDTO roles;
}
