package com.example.auth.user.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.auth.user.dto.UserDTO;
import com.example.auth.user.entity.UserEntity;
import com.example.auth.user.service.UserService;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
public class UserController {

	@Autowired
	private UserService userService;

	// @Secured({"ROLE_ADMIN", "ROLE_USER"})
	@PreAuthorize("hasRole('ADMIN')")
	@RequestMapping(value = "/user", method = RequestMethod.GET)
	public List<UserEntity> listUser() {
		return userService.findAll();
	}

	// @Secured("ROLE_USER")
	// @PreAuthorize("hasRole('USER')")
	@PreAuthorize("hasAnyRole('USER', 'ADMIN')")
	@RequestMapping(value = "/user/{id}", method = RequestMethod.GET)
	public UserEntity getOne(@PathVariable(value = "id") Long id) {
		return userService.findById(id);
	}

	@PostMapping(value = "/signup")
	public UserEntity create(HttpServletRequest request, @RequestBody UserDTO user) {
		return userService.save(user);
	}

	@PreAuthorize("hasRole('ADMIN')")
	@RequestMapping(value = "/user/{id}", method = RequestMethod.DELETE)
	public Long deleteUser(@PathVariable(value = "id") Long id) {
		userService.delete(id);
		return new Long(id);
	}
}
