package com.example.auth.user.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name="roles")
@Getter
@Setter
public class RoleEntity {

	@Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long roleId;

    @Column
    private String name;

    @Column
    private String description;
    
    @OneToMany(mappedBy="roles", cascade=CascadeType.ALL ,fetch=FetchType.EAGER)
    @JsonBackReference
    private List<UserEntity> userEntities ;

}
