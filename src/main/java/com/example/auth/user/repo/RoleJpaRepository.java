package com.example.auth.user.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.auth.user.entity.RoleEntity;

//@Repository
public interface RoleJpaRepository extends JpaRepository<RoleEntity, Long>{ 

}
