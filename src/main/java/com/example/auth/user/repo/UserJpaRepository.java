package com.example.auth.user.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.auth.user.entity.UserEntity;

//@Repository
public interface UserJpaRepository extends JpaRepository<UserEntity, Long> {

	@Query("from UserEntity u where u.username=?1")
	UserEntity findByUsername(String username);

}
