package com.example.auth.user.service.imp;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.example.auth.user.dto.UserDTO;
import com.example.auth.user.entity.RoleEntity;
import com.example.auth.user.entity.UserEntity;
import com.example.auth.user.repo.RoleJpaRepository;
import com.example.auth.user.repo.UserJpaRepository;
import com.example.auth.user.service.UserService;

@Service("userService")
public class UserServiceImpl implements UserService, UserDetailsService {

	@Resource
	private UserJpaRepository userRepo;

	@Resource
	private RoleJpaRepository roleRepo;

	@Autowired
	private BCryptPasswordEncoder bcryptEncoder;

	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		UserEntity user = userRepo.findByUsername(username);
		if (user == null) {
			throw new UsernameNotFoundException("Invalid username or password.");
		}
		return new User(user.getUsername(), user.getPassword(), getAuthority(user));
	}

	private Set<SimpleGrantedAuthority> getAuthority(UserEntity user) {
		Set<SimpleGrantedAuthority> authorities = new HashSet<>();
		authorities.add(new SimpleGrantedAuthority("ROLE_" + user.getRoles().getName()));
		return authorities;
	}

	public List<UserEntity> findAll() {
		List<UserEntity> list = new ArrayList<>();
		userRepo.findAll().iterator().forEachRemaining(list::add);
		return list;
	}

	@Override
	public UserEntity findOne(String username) {
		return userRepo.findByUsername(username);
	}

	@Override
	public UserEntity findById(Long id) {
		return userRepo.findById(id).get();
	}

	@Override
	public UserEntity save(UserDTO user) {
		return userRepo.save(convrerUserDtototEntity(user));
	}

	private UserEntity convrerUserDtototEntity(UserDTO user) {
		UserEntity newUser = new UserEntity();
		newUser.setUsername(user.getUserName());
		newUser.setPassword(bcryptEncoder.encode(user.getPassword()));
		newUser.setAge(user.getAge());
		newUser.setSalary(user.getSalary());
		Optional<RoleEntity> rolesCheck = roleRepo.findById(user.getRoles().getId());
		if (rolesCheck.isPresent())
			newUser.setRoles(rolesCheck.get());
		return newUser;
	}

	@Override
	public void delete(Long id) {
		userRepo.deleteById(id);
	}

}
