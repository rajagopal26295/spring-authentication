package com.example.auth.user.service;

import java.util.List;

import com.example.auth.user.dto.UserDTO;
import com.example.auth.user.entity.UserEntity;

public interface UserService {

	List<UserEntity> findAll();

	UserEntity findById(Long id);

	UserEntity save(UserDTO user);

	void delete(Long id);

	UserEntity findOne(String username);

}
