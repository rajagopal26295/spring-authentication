package com.example.auth.jwt.filter;

import java.util.Collections;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import com.google.common.base.Predicate;
import com.google.common.base.Predicates;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
@Profile("dev")
public class SwaggerConfig{

	@Bean
	public Docket api() {
		return new Docket(DocumentationType.SWAGGER_2)
				.apiInfo(apiInfo())
				.select()
				.apis( RequestHandlerSelectors.basePackage("com.auth"))
				.apis(Predicates.not(RequestHandlerSelectors.basePackage("org.springframework.boot")))
				.paths(paths())
				.build();
    }
	
	private ApiInfo apiInfo(){
		return new ApiInfo("Authentication Server", "Authentication and Authorization for Users", "2.1.4", "Terms of Use", new Contact("MRG", "www.google.com", "rajagopal.mahendran@mindzen.co.in"), "Google License", "www.google.com/license", Collections.emptyList());
	}
	
	private Predicate<String> paths() {
        return Predicates.not(PathSelectors.regex("/error/.*"));
    }
}
